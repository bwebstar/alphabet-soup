var textarea = document.querySelector('textarea');

$.ajax({
  url: 'input.txt',
  cache: false,
  type: 'text',
  method: 'GET',
}).done(function (data) {
  var wordSearchObject = parseTextSearch(data);

  var matches = findMatches(wordSearchObject);

  var lines = exportToText(matches);

  textarea.value = lines;
});

function parseTextSearch(text) {
  var splitText = text.split(/\r\n|\n/);

  if (splitText.length > 1) {
    var firstLine = String(splitText[0].split('\n'));

    var rows = parseInt(firstLine.charAt(0));

    splitText.shift();

    var grid = new Array(rows);

    var rawLines = splitText.slice(0, rows);

    var rawWords = splitText.slice(rows);

    rawWords = sanitizeArr(rawWords);

    if (rawLines.length > 0 && rawWords.length > 0) {
      var wsObject = {
        grid: rawLines,
        words: rawWords,
      };
      return wsObject;
    }
  }
  return { grid: [], words: [] };
}

function exportToText(matches) {
  var wordProperties = Object.keys(matches);

  var matchingResults = wordProperties.join('\n');

  return matchingResults;
}

function findMatches(ws) {
  var allowMultiple = false;

  var matches = {};
  for (var i = 0; i < ws.words.length; i++) {
    var thisWord = ws.words[i];
    var wordMatches = findWord(ws, thisWord, allowMultiple);
    matches[thisWord] = wordMatches;
  }
  return matches;
}

function findWord(ws, word, allowMultiple) {
  var firstLetterPositions = getCharPositions(ws, word[0]);

  var matches = [];

  var matchTemplate = {
    word: word,
    firstLetterPosition: null,
    lastLetterPosition: null,
    currentLetterIndex: 0,
    searchDirection: null,
    success: false,
  };

  for (var i = 0; i < firstLetterPositions.length; i++) {
    var matchObject = $.extend(true, {}, matchTemplate);
    matchObject.firstLetterPosition = firstLetterPositions[i];
    matchObject.lastLetterPosition = firstLetterPositions[i];
    matchObject.currentLetterIndex = 1;

    for (var i2 = 0; i2 < 8; i2++) {
      matchObject.searchDirection = i2;
      if (searchRecursive(ws, matchObject)) {
        matchObject.success = true;
        matches.push(matchObject);

        if (allowMultiple) {
          break;
        } else {
          return matches;
        }
      }
    }
  }

  if (matches.length == 0) {
    var matchObject = $.extend(true, {}, matchTemplate);
    matches.push(matchObject);
  }

  return matches;
}

function searchRecursive(ws, mo) {
  var grid = ws.grid;

  var testData = null;
  switch (mo.searchDirection) {
    case 0:
      var testData = getSafeCharAt(
        ws,
        mo.lastLetterPosition.line - 1,
        mo.lastLetterPosition.index - 1
      );
      break;
    case 1:
      var testData = getSafeCharAt(
        ws,
        mo.lastLetterPosition.line - 1,
        mo.lastLetterPosition.index
      );
      break;
    case 2:
      var testData = getSafeCharAt(
        ws,
        mo.lastLetterPosition.line - 1,
        mo.lastLetterPosition.index + 1
      );
      break;
    case 3:
      var testData = getSafeCharAt(
        ws,
        mo.lastLetterPosition.line,
        mo.lastLetterPosition.index + 1
      );
      break;
    case 4:
      var testData = getSafeCharAt(
        ws,
        mo.lastLetterPosition.line + 1,
        mo.lastLetterPosition.index + 1
      );
      break;
    case 5:
      var testData = getSafeCharAt(
        ws,
        mo.lastLetterPosition.line + 1,
        mo.lastLetterPosition.index
      );
      break;
    case 6:
      var testData = getSafeCharAt(
        ws,
        mo.lastLetterPosition.line + 1,
        mo.lastLetterPosition.index - 1
      );
      break;
    case 7:
      var testData = getSafeCharAt(
        ws,
        mo.lastLetterPosition.line,
        mo.lastLetterPosition.index - 1
      );
      break;
  }

  if (
    mo.word[mo.currentLetterIndex] &&
    testData.char &&
    mo.word[mo.currentLetterIndex].toUpperCase() == testData.char.toUpperCase()
  ) {
    mo.lastLetterPosition = testData;
    if (mo.word.length == mo.currentLetterIndex + 1) {
      return true;
    } else {
      mo.currentLetterIndex += 1;
      return searchRecursive(ws, mo);
    }
  } else {
    mo.lastLetterPosition = mo.firstLetterPosition;
    mo.currentLetterIndex = 1;
    return false;
  }
}

function getCharPositions(ws, char) {
  var positions = [];
  for (var i = 0; i < ws.grid.length; i++) {
    var line = ws.grid[i];
    for (var i2 = 0; i2 < line.length; i2++) {
      if (line[i2] && char && line[i2].toUpperCase() == char.toUpperCase())
        positions.push({ char: char, line: i, index: i2 });
    }
  }

  return positions;
}

function getSafeCharAt(ws, lineIndex, index) {
  var line = ws.grid[lineIndex];
  if (line) {
    var char = line[index];
    if (char) {
      return { char: char, line: lineIndex, index: index };
    }
  }
  return false;
}

function sanitizeArr(array) {
  return array.filter(function (n) {
    return n != '';
  });
}
